package com.my_project.my_pro.shoppingcard;

import com.my_project.my_pro.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class ShoppingCardItemServiceImpl implements ShoppingCardItemService {

    private ShoppingCartItemRepo shoppingCartItemRepo;

    @Autowired
    public ShoppingCardItemServiceImpl(ShoppingCartItemRepo shoppingCartItemRepo) {
        this.shoppingCartItemRepo = shoppingCartItemRepo;
    }

    @Override
    public ShoppingCartItem save(ShoppingCartItem item) {
        return shoppingCartItemRepo.save(item);
    }

    @Override
    public void addItem(Product product){
        //
    }
}
