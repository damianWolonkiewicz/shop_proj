package com.my_project.my_pro.shoppingcard;

import com.my_project.my_pro.product.Product;

public interface ShoppingCardItemService {

    ShoppingCartItem save(ShoppingCartItem item);

    void addItem(Product product);

}
