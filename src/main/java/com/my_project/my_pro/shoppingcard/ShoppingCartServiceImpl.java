package com.my_project.my_pro.shoppingcard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
class ShoppingCartServiceImpl implements ShoppingCartService {

    private final ShoppingCartRepo shoppingCartRepo;
    private final ShoppingCardItemService shoppingCartItemService;

    @Autowired
    public ShoppingCartServiceImpl(ShoppingCartRepo shoppingCartRepo, ShoppingCardItemService shoppingCartItemService) {
        this.shoppingCartRepo = shoppingCartRepo;
        this.shoppingCartItemService = shoppingCartItemService;
    }

    @Override
    public ShoppingCart save(ShoppingCart shoppingCart) {
        return shoppingCartRepo.save(shoppingCart);
    }

    // TODO da sad asd
    @Override
    public ShoppingCart update(ShoppingCart shoppingCart) {
        return shoppingCartRepo.save(shoppingCart);
    }

    @Override
    public void addToCart(ShoppingCartItem shoppingCartItem) {
        shoppingCartItemService.save(shoppingCartItem);
    }

    @Override
    public void deleteFromCart(int id) {

    }

    @Override
    public void delete(int id) {

    }

    @Override
    public ShoppingCart findById(int id) {
        return null;
    }
}
