package com.my_project.my_pro.shoppingcard;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShoppingCartItemRepo extends CrudRepository<ShoppingCartItem, Integer> {


}
