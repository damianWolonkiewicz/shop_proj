package com.my_project.my_pro.shoppingcard;

public interface ShoppingCartService {

    ShoppingCart save(ShoppingCart shoppingCart);

    ShoppingCart update(ShoppingCart shoppingCart);

    void addToCart(ShoppingCartItem shoppingCartItem);

    void deleteFromCart(int id);

    void delete(int id);

    ShoppingCart findById(int id);

}
