package com.my_project.my_pro.shoppingcard;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
//@Builder
public class ShoppingCart implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

//    @OneToMany(mappedBy = "shoppingCartItem", fetch = FetchType.EAGER)
    @OneToMany
    private List<ShoppingCartItem> cartItems = new ArrayList<>();

    public ShoppingCart() {
    }

    public ShoppingCart(List<ShoppingCartItem> cartItems) {
        this.cartItems = cartItems;
    }

}
