package com.my_project.my_pro.shoppingcard;

import com.my_project.my_pro.product.Product;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

@Entity
//@Builder
@Data
public class ShoppingCartItem implements Serializable {

    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @JoinColumn(name = "product_id")
    @OneToMany
    private Product product;

    @NotNull
    private int quantity;

    @NotNull
    private double totalPrice;

    public ShoppingCartItem() {
    }

    public ShoppingCartItem(Product product, int quantity, double totalPrice) {
        this.product = product;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ShoppingCartItem)) return false;
        ShoppingCartItem that = (ShoppingCartItem) o;
        return id == that.id &&
                quantity == that.quantity &&
                Double.compare(that.totalPrice, totalPrice) == 0 &&
                Objects.equals(product, that.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, product, quantity, totalPrice);
    }
}
