package com.my_project.my_pro.ui_frontend;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class MyService {
    public String sayHi() {
        return "Hello there " + LocalDateTime.now();
    }
}
