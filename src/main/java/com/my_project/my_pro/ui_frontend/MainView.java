package com.my_project.my_pro.ui_frontend;


import com.my_project.my_pro.category.CategoryServiceImpl;
import com.my_project.my_pro.product.Product;
import com.my_project.my_pro.product.ProductServiceImpl;
import com.my_project.my_pro.user.User;
import com.my_project.my_pro.user.UserServiceImpl;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route(value = "home")
public class MainView extends VerticalLayout {

    private Button myButton = new Button("Click me");

    private final UserServiceImpl userService;
    private final ProductServiceImpl productService;

    final Grid<User> userGrid;
    final Grid<Product> productGrid;

    public MainView(MyService service, UserServiceImpl userService, ProductServiceImpl productService) {
        this.userService = userService;
        this.productService = productService;
        this.userGrid = new Grid<>(User.class);
        this.productGrid = new Grid<>(Product.class);
        add(productGrid, myButton);
        listProducts();

        myButton.addClickListener(event -> Notification.show(service.sayHi()));
    }

    private void listUsers() {
        userGrid.setItems(userService.findAll());
    }

    private void listProducts() {
        productGrid.setItems(productService.findAll());
    }

}

