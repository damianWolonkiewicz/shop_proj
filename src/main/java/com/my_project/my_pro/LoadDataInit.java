package com.my_project.my_pro;

import com.my_project.my_pro.category.Category;
import com.my_project.my_pro.category.CategoryService;
import com.my_project.my_pro.product.Product;
import com.my_project.my_pro.product.ProductService;
import com.my_project.my_pro.shoppingcard.ShoppingCardItemService;
import com.my_project.my_pro.shoppingcard.ShoppingCartService;
import com.my_project.my_pro.user.User;
import com.my_project.my_pro.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
class LoadDataInit implements CommandLineRunner {

    private CategoryService categoryService;
    private ProductService productService;
    private ShoppingCartService shoppingCartService;
    private ShoppingCardItemService shoppingCardItemService;
    private UserService userService;

    @Autowired
    public LoadDataInit(CategoryService categoryService, ProductService productService, ShoppingCartService shoppingCartService, ShoppingCardItemService shoppingCardItemService, UserService userService) {
        this.categoryService = categoryService;
        this.productService = productService;
        this.shoppingCartService = shoppingCartService;
        this.shoppingCardItemService = shoppingCardItemService;
        this.userService = userService;
    }

    @Override
    public void run(String... args) throws Exception {

        User user1 = new User("Andrzej Duda", "bolek123", true);
        User user2 = new User("Bolek", "duda123", true);
        User user3 = new User("Janusz", "jkm123", true);

        userService.save(user1);
        userService.save(user2);
        userService.save(user3);

        Category nabial = new Category("Nabiał");
        Category pieczywo = new Category("Pieczywo");
        Category napoje = new Category("Napoje");

        Product maslo = new Product("Masło",
                9.99, "82% tłuszczu",
                Collections.singleton(nabial));

        Product chleb = new Product("Chleb",
                4.99, "Like homemade",
                Collections.singleton(pieczywo));

        Product woda = new Product("Woda",
                1.99, "Woda niegazowana",
                Collections.singleton(napoje));

        Product sok = new Product("Sok pomarańczowy",
                9.99, "Sok pomarańczowy 100%",
                Collections.singleton(napoje));

        categoryService.save(nabial);
        categoryService.save(pieczywo);
        categoryService.save(napoje);

        productService.save(maslo);
        productService.save(chleb);
        productService.save(woda);
        productService.save(sok);
    }
}
