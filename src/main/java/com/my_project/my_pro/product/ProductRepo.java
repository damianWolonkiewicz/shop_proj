package com.my_project.my_pro.product;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
interface ProductRepo extends CrudRepository<Product, Integer> {
}
