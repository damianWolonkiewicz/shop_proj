package com.my_project.my_pro.product;

import java.util.List;

public interface ProductService {

    Product save(Product product);

    Product update(Product product);

    void delete(int id);

    Product findById(int id);

    List<Product> findAll();


}
