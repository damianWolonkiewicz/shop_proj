package com.my_project.my_pro.product;

import com.my_project.my_pro.category.Category;
import com.my_project.my_pro.shoppingcard.ShoppingCart;
import com.my_project.my_pro.shoppingcard.ShoppingCartItem;
import lombok.Builder;
import lombok.Data;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
//@Builder
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private double price;

    private String description;

    // OK
    @ManyToMany()
    @JoinTable(joinColumns = {@JoinColumn(name = "fk_category")},
            inverseJoinColumns = {@JoinColumn(name = "fk_product")})
    private Set<Category> category = new HashSet<>();

    // OK?
//    @OneToMany
//    @JoinTable(joinColumns = {@JoinColumn(name = "fk_product")},
//            inverseJoinColumns = {@JoinColumn(name = "fk_shopping_cart")})
//    private Set<ShoppingCartItem> shoppingCartItems = new HashSet<>();

//    @ManyToMany
//    @JoinTable(joinColumns = {@JoinColumn(name = "fk_product")},
//            inverseJoinColumns = {@JoinColumn(name = "fk_shopping_cart")})
//    private Set<ShoppingCartItem> shoppingCartItems = new HashSet<>();

    public Product() {
    }

    public Product(String name, double price, String description, Set<Category> category) {
        this.name = name;
        this.price = price;
        this.description = description;
        this.category = category;
    }
}
