package com.my_project.my_pro.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepo productRepo;

    @Autowired
    ProductServiceImpl(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @Override
    public Product save(Product product) {
        return productRepo.save(product);
    }

    // TODO jakbasdjkakd
    @Override
    public Product update(Product product) {
        return productRepo.save(product);
    }

    @Override
    public void delete(int id) {
        productRepo.delete(findById(id));
    }

    @Override
    public Product findById(int id) {
        return productRepo.findById(id).orElseThrow(
                () -> new RuntimeException("Prod not found")
        );
    }

    @Override
    public List<Product> findAll() {
        List<Product> products = new ArrayList<>();
        productRepo.findAll().forEach(products::add);
        return products;
    }
}
