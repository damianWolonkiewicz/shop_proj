package com.my_project.my_pro.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;

    @Autowired
    UserServiceImpl(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public User findUserById(int id) {
        return userRepo.findById(id).orElseThrow(() ->
                new RuntimeException("User not found")
        );
    }

    @Override
    public User save(User user) {
        return userRepo.save(user);
    }

    // TODO asdas
    @Override
    public User update(User user) {
        return userRepo.save(user);
    }

    @Override
    public void delete(int id) {
        userRepo.delete(findUserById(id));
    }

    @Override
    public Stream<User> findAll() {
        List<User> users = new ArrayList<>();

        userRepo.findAll().forEach(users::add);

        return users.stream();
    }


}
