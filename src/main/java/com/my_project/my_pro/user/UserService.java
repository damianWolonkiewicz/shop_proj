package com.my_project.my_pro.user;

import java.util.stream.Stream;

public interface UserService {

    User findUserById(int id);

    User save(User user);

    User update(User user);

    void delete(int id);

    Stream<User> findAll();

}
