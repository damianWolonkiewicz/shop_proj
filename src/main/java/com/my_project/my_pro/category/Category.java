package com.my_project.my_pro.category;

import com.my_project.my_pro.product.Product;
import lombok.Builder;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Data
//@Builder
@Entity
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;


    @ManyToMany(mappedBy = "category", fetch = FetchType.EAGER)
    private Set<Product> product;

    public Category() {
    }

    public Category( String name) {
        this.name = name;
    }
}
