package com.my_project.my_pro.category;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepo categoryRepo;

    @Autowired
    public CategoryServiceImpl(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @Override
    public Category save(Category category) {
        return categoryRepo.save(category);
    }

    // TODO Może wystarczy metoda save()!!
    @Override
    public Category update(Category category) {
        return categoryRepo.save(category);
    }

    @Override
    public void delete(int id) {
        categoryRepo.delete(findById(id));
    }

    @Override
    public Category findById(int id) {
        return categoryRepo.findById(id).orElseThrow(() ->
                new RuntimeException("Category not found")
        );
    }

    @Override
    public Stream<Category> findAll() {
        List<Category> categories = new ArrayList<>();
        categoryRepo.findAll().forEach(categories::add);
        return categories.stream();
    }

}
