package com.my_project.my_pro.category;

import java.util.stream.Stream;

public interface CategoryService {

    Category save(Category category);

    Category update(Category category);

    void delete(int id);

    Category findById(int id);

    Stream<Category> findAll();

}
